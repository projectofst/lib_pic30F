/**
 *! \file timer.h
 *  \brief Provides hardware timers
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#ifndef __TIMER_H__
#define __TIMER_H__
#include "timing.h"


/*! \def set_priority_timer1(priority)
 * \brief Set timer 1 priority. Priority can be defined between 0 and 7 where 0
 * is the minimum priority
 * \param priority priority to set
 */
#define set_priority_timer1(priority) do{IPC0bits.T1IP = priority;} while (0)
/*! \def set_priority_timer2(priority)
 * \brief Set timer 2 priority. Priority can be defined between 0 and 7 where 0
 * is the minimum priority
 * \param priority priority to set
 */
#define set_priority_timer2(priority) do{IPC1bits.T2IP = priority;} while (0)
/*! \def set_priority_timer3(priority)
 * \brief Set timer 3 priority. Priority can be defined between 0 and 7 where 0
 * is the minimum priority
 * \param priority priority to set
 */
#define set_priority_timer3(priority) do{IPC1bits.T3IP = priority;} while (0)
/*! \def set_priority_timer4(priority)
 * \brief Set timer 4 priority. Priority can be defined between 0 and 7 where 0
 * is the minimum priority
 * \param priority priority to set
 */
#define set_priority_timer4(priority) do{IPC5bits.T4IP = priority;} while (0)
/*! \def set_priority_timer5(priority)
 * \brief Set timer 5 priority. Priority can be defined between 0 and 7 where 0
 * is the minimum priority
 * \param priority priority to set
 */
#define set_priority_timer5(priority) do{IPC5bits.T5IP = priority;} while (0)


/*! \def start_timer1()
 * \brief Start timer 1
 */
#define start_timer1() do{T1CONbits.TON = 1;}while(0);
/*! \def start_timer2()
 * \brief Start timer 2
 */
#define start_timer2() do{T2CONbits.TON = 1;}while(0);
/*! \def start_timer3()
 * \brief Start timer 3
 */
#define start_timer3() do{T3CONbits.TON = 1;}while(0);
/*! \def start_timer4()
 * \brief Start timer 4
 */
#define start_timer4() do{T4CONbits.TON = 1;}while(0);
/*! \def start_timer5()
 * \brief Start timer 5
 */
#define start_timer5() do{T5CONbits.TON = 1;}while(0);


/*! \def stop_timer1()
 * \brief Stop timer 1.
 */
#define stop_timer1() do{T1CONbits.TON = 0;}while(0);
/*! \def stop_timer2()
 * \brief Stop timer 2.
 */
#define stop_timer2() do{T2CONbits.TON = 0;}while(0);
/*! \def stop_timer3()
 * \brief Stop timer 3.
 */
#define stop_timer3() do{T3CONbits.TON = 0;}while(0);
/*! \def stop_timer4()
 * \brief Stop timer 4.
 */
#define stop_timer4() do{T4CONbits.TON = 0;}while(0);
/*! \def stop_timer5()
 * \brief Stop timer5.
 */
#define stop_timer5() do{T5CONbits.TON = 0;}while(0);


/*! \def set_period_timer1(time)
 * \brief Set timer 1
 * \param time Period of the timer in milliseconds
 */
#define set_period_timer1(time) do{PR1 = (M_SEC/256)*time;}while(0);
/*! \def set_period_timer2(time)
 * \brief Set timer 2
 * \param time Period of the timer in milliseconds
 */
#define set_period_timer2(time) do{PR2 = (M_SEC/256)*time;}while(0);
/*! \def set_period_timer3(time)
 * \brief Set timer 3
 * \param time Period of the timer in milliseconds
 */
#define set_period_timer3(time) do{PR3 = (M_SEC/256)*time;}while(0);
/*! \def set_period_timer4(time)
 * \brief Set timer 4
 * \param time Period of the timer in milliseconds
 */
#define set_period_timer4(time) do{PR4 = (M_SEC/256)*time;}while(0);
/*! \def set_period_timer5(time)
 * \brief Set timer 5
 * \param time Period of the timer in milliseconds
 */
#define set_period_timer5(time) do{PR5 = (M_SEC/256)*time;}while(0);


/*! \def set_raw_period_timer1(time)
 * \brief Set timer 1 period. Raw definition
 * \param time Number of counts of the timer
 */
#define set_raw_period_timer1(time) do{PR1 = time;}while(0);
/*! \def set_raw_period_timer2(time)
 * \brief Set timer 2 period. Raw definition
 * \param time Number of counts of the timer
 */
#define set_raw_period_timer2(time) do{PR2 = time;}while(0);
/*! \def set_raw_period_timer3(time)
 * \brief Set timer 3 period. Raw definition
 * \param time Number of counts of the timer
 */
#define set_raw_period_timer3(time) do{PR3 = time;}while(0);
/*! \def set_raw_period_timer4(time)
 * \brief Set timer 4 period. Raw definition
 * \param time Number of counts of the timer
 */
#define set_raw_period_timer4(time) do{PR4 = time;}while(0);
/*! \def set_raw_period_timer5(time)
 * \brief Set timer 5 period. Raw definition
 * \param time Number of counts of the timer
 */
#define set_raw_period_timer5(time) do{PR5 = time;}while(0);


/*! \fn void config_timer1(unsigned int time, unsigned int priority, 
		void (*timer_function)(void));
 * \brief Configurate time 1.
 * \param time Period in milliseconds
 * \param priority Interrupt priority
 * \param timer_function Funtion that runs in each interrupt
 */
void config_timer1(unsigned int time, unsigned int priority, 
		void (*timer_function)(void));
/*! \fn void config_timer2(unsigned int time, unsigned int priority, 
		void (*timer_function)(void));
 * \brief Configurate time 2.
 * \param time Period in milliseconds
 * \param priority Interrupt priority
 * \param timer_function Funtion that runs in each interrupt
 */
void config_timer2(unsigned int time, unsigned int priority, 
		void (*timer_function)(void));
/*! \fn void config_timer3(unsigned int time, unsigned int priority, 
		void (*timer_function)(void));
 * \brief Configurate time 3.
 * \param time Period in milliseconds
 * \param priority Interrupt priority
 * \param timer_function Funtion that runs in each interrupt
 */
void config_timer3(unsigned int time, unsigned int priority, 
		void (*timer_function)(void));
/*! \fn void config_timer4(unsigned int time, unsigned int priority, 
		void (*timer_function)(void));
 * \brief Configurate time 4.
 * \param time Period in milliseconds
 * \param priority Interrupt priority
 * \param timer_function Funtion that runs in each interrupt
 */
void config_timer4(unsigned int time, unsigned int priority, 
		void (*timer_function)(void));
/*! \fn void config_timer5(unsigned int time, unsigned int priority, 
		void (*timer_function)(void));
 * \brief Configurate time 5.
 * \param time Period in milliseconds
 * \param priority Interrupt priority
 * \param timer_function Funtion that runs in each interrupt
 */
void config_timer5(unsigned int time, unsigned int priority, 
		void (*timer_function)(void));

#endif
