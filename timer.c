/**********************************************************************
 *   lib_pic30f
 *
 *   Timer
 *      - module configuration
 *      - interruption assignment
 *   ______________________________________________________________
 *
 *   Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *   This program is free software; you can redistribute it and/or
 *   modify it under the terms of the GNU General Public License
 *   as published by the Free Software Foundation; version 2 of the
 *   License only.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/


#include <xc.h>
#include <limits.h>
#include <stdlib.h>

#include "timer.h"

void (*fun_ptr_timer1)(void);
void (*fun_ptr_timer2)(void);
void (*fun_ptr_timer3)(void);
void (*fun_ptr_timer4)(void);
void (*fun_ptr_timer5)(void);

void default_timer(void) {
	return;
}

/**********************************************************************
 * Name:    config_timer1
 * Args:    time - interrupt step in miliseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer1(unsigned int time, unsigned int priority, void (*timer_function)(void)){

	T1CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T1CONbits.TGATE = 0;		/* Gated mode off                        */
	T1CONbits.TCKPS = 3;		/* prescale 1:256                        */
	T1CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR1 = 0;					/* clears the timer register             */
	PR1 = (M_SEC/256)*time;		/* value at which the register overflows */
								/* and raises T1IF                       */

	/* interruptions */
	IPC0bits.T1IP = priority;	/* Timer 1 Interrupt Priority 0-7        */
	IFS0bits.T1IF = 0;			/* clear interrupt flag                  */
	IEC0bits.T1IE = 1;			/* Timer 1 Interrupt Enable              */

	fun_ptr_timer1 = (timer_function == NULL) ? default_timer : timer_function;

	T1CONbits.TON = 1;			/* starts the timer                      */
	return;
}



/**********************************************************************
 * Name:    config_timer2
 * Args:    time - interrupt step in miliseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer2(unsigned int time, unsigned int priority, void (*timer_function)(void)){

	T2CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T2CONbits.TGATE = 0;		/* Gated mode off                        */
	T2CONbits.TCKPS = 3;		/* prescale 1:256                         */
	T2CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR2 = 0;					/* clears the timer register             */
	PR2 = (M_SEC/256)*time;		/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC1bits.T2IP = priority;	/* Timer 2 Interrupt Priority 0-7        */
	IFS0bits.T2IF = 0;			/* clear interrupt flag                  */
	IEC0bits.T2IE = 1;			/* Timer 2 Interrupt Enable              */

	fun_ptr_timer2 = (timer_function == NULL) ? default_timer : timer_function;

	T2CONbits.TON = 1;			/* starts the timer                      */
	return;
}


/**********************************************************************
 * Name:    config_timer3
 * Args:    time - interrupt step in miliseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer3(unsigned int time, unsigned int priority, void (*timer_function)(void)){

	T3CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T3CONbits.TGATE = 0;		/* Gated mode off                        */
	T3CONbits.TCKPS = 3;		/* prescale 1:256                        */
	T3CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR3 = 0;					/* clears the timer register             */
	PR3 = (M_SEC/256)*time;		/* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC1bits.T3IP = priority;	/* Timer 3 Interrupt Priority 0-7        */
	IFS0bits.T3IF = 0;			/* clear interrupt flag                  */
	IEC0bits.T3IE = 1;			/* Timer 3 Interrupt Enable              */

	fun_ptr_timer3 = (timer_function == NULL) ? default_timer : timer_function;

	T3CONbits.TON = 1;			/* starts the timer                      */
	return;
}


/**********************************************************************
 * Name:    config_timer4
 * Args:    time - interrupt step in miliseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer4(unsigned int time, unsigned int priority, void (*timer_function)(void)){

	T4CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T4CONbits.TGATE = 0;		/* Gated mode off                        */
	T4CONbits.TCKPS = 3;		/* prescale 1:256                        */
	T4CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR4 = 0;					/* clears the timer register             */
	PR4 = (M_SEC/256)*time;	    /* value at which the register overflows *
								 * and raises T1IF                       */

	/* interruptions */
	IPC5bits.T4IP = priority;	/* Timer 4 Interrupt Priority 0-7        */
	IFS1bits.T4IF = 0;			/* clear interrupt flag                  */
	IEC1bits.T4IE = 1;			/* Timer 4 Interrupt Enable              */

	fun_ptr_timer4 = (timer_function == NULL) ? default_timer : timer_function;

	T4CONbits.TON = 1;			/* starts the timer                      */
	return;
}


/**********************************************************************
 * Name:    config_timer5
 * Args:    time - interrupt step in miliseconds; priority 0-7
 * Return:  -
 * Desc:    Configures and enables timer 1 module.
 **********************************************************************/
void config_timer5(unsigned int time, unsigned int priority, void (*timer_function)(void)){

	T5CONbits.TCS   = 0;		/* use internal clock: Fcy               */
	T5CONbits.TGATE = 0;		/* Gated mode off                        */
	T5CONbits.TCKPS = 3;		/* prescale 1:256                        */
	T5CONbits.TSIDL = 0;		/* don't stop the timer in idle          */

	TMR5 = 0;					/* clears the timer register             */
	PR5 = (M_SEC/256)*time;		/* value at which the register overflows *
							     * and raises T1IF                       */

	/* interruptions */
	IPC5bits.T5IP = priority;	/* Timer 5 Interrupt Priority 0-7        */
	IFS1bits.T5IF = 0;			/* clear interrupt flag                  */
	IEC1bits.T5IE = 1;			/* Timer 5 Interrupt Enable              */

	fun_ptr_timer5 = (timer_function == NULL) ? default_timer : timer_function;

	T5CONbits.TON = 1;			/* starts the timer                      */
	return;
}

 
/**********************************************************************
 * Assign Timer 1 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T1Interrupt (void){
	fun_ptr_timer1();
	IFS0bits.T1IF = 0;			/* clear interrupt flag                  */                 

	return;
}

/**********************************************************************
 * Assign Timer 2 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T2Interrupt (void){
	fun_ptr_timer2();
	IFS0bits.T2IF = 0;			/* clear interrupt flag                  */

	return;
}

/**********************************************************************
 * Assign Timer 3 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T3Interrupt (void){
	fun_ptr_timer3();
	IFS0bits.T3IF = 0;			/* clear interrupt flag                  */

	return;
}
/**********************************************************************
 * Assign Timer 4 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T4Interrupt (void){
	fun_ptr_timer4();
	IFS1bits.T4IF = 0;			/* clear interrupt flag                  */

	return;
}
/**********************************************************************
 * Assign Timer 5 interruption
 **********************************************************************/
void __attribute__((interrupt, auto_psv, shadow)) _T5Interrupt (void){
	fun_ptr_timer5();
	IFS1bits.T5IF = 0;			/* clear interrupt flag                  */
	
	return;
}
